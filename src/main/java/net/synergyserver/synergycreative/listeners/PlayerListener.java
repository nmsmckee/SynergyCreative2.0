package net.synergyserver.synergycreative.listeners;

import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.events.SettingChangeEvent;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycreative.SynergyCreative;
import net.synergyserver.synergycreative.inputs.Input;
import net.synergyserver.synergycreative.inputs.InputFunction;
import net.synergyserver.synergycreative.inputs.InputManager;
import net.synergyserver.synergycreative.inputs.WirelessInputFunction;
import net.synergyserver.synergycreative.settings.CreativeToggleSetting;
import net.synergyserver.synergycreative.utils.PlotUtil;
import org.bukkit.Bukkit;
import org.bukkit.GameRule;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Levelled;
import org.bukkit.block.data.type.Slab;
import org.bukkit.craftbukkit.v1_19_R1.block.impl.CraftLayeredCauldron;
import org.bukkit.craftbukkit.v1_19_R1.block.impl.CraftPowderSnowCauldron;
import org.bukkit.craftbukkit.v1_19_R1.block.impl.CraftShulkerBox;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.List;

/**
 * Listens to all player-related events that are not covered by other listeners.
 */
public class PlayerListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // If drops is set to false then cancel the event
        if (!CreativeToggleSetting.DROPS.getValue(mcp)) {
            event.getItemDrop().remove();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerBreakBlock(BlockBreakEvent event) {
        Block block = event.getBlock();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Ignore if not an inventory block
        if(!(block.getState() instanceof InventoryHolder)){
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(event.getPlayer());
        // If drops is set to false then cancel the event and clear the inventory before breaking
        if (!CreativeToggleSetting.DROPS.getValue(mcp)) {
            event.setCancelled(true);
            ((InventoryHolder) block.getState()).getInventory().clear();
            block.getState().update();
            block.breakNaturally();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerKillMinecart(VehicleDestroyEvent event) {
        Vehicle vehicle = event.getVehicle();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(vehicle.getWorld().getName())) {
            return;
        }

        // Ignore if it wasn't a player that broke the vehicle
        if(!(event.getAttacker() instanceof Player)){
            return;
        }

        // Ignore the event if the vehicle isn't an inventory holder
        if (!(vehicle instanceof InventoryHolder)) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile((Player) event.getAttacker());
        // If drops is set to false then cancel the event and clear the inventory before breaking
        if (!CreativeToggleSetting.DROPS.getValue(mcp)) {
            event.setCancelled(true);
            ((InventoryHolder) vehicle).getInventory().clear();
            vehicle.remove();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(player);

        // Delay it shortly because Bukkit is stupid
        Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), new Runnable() {
            @Override
            public void run() {
                // If night_vision is set to true then give them night vision
                if (CreativeToggleSetting.NIGHT_VISION.getValue(mcp)) {
                    // Fix for multiverse being stupid
                    if (player.hasPotionEffect(PotionEffectType.NIGHT_VISION)) {
                        player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                    }
                    player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false));
                } else {
                    player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                }
            }
        }, 1L);

    }


    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerSettingChange(SettingChangeEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        if (event.getSetting().equals(CreativeToggleSetting.NIGHT_VISION)) {
            // Either remove or give them the night vision effect, depending on the new value
            if ((boolean) event.getNewValue()) {
                // Fix for multiverse being stupid
                if (player.hasPotionEffect(PotionEffectType.NIGHT_VISION)) {
                    player.removePotionEffect(PotionEffectType.NIGHT_VISION);
                }
                player.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, Integer.MAX_VALUE, 0, false, false));
            } else {
                player.removePotionEffect(PotionEffectType.NIGHT_VISION);
            }
//        } else if (event.getSetting().equals(CreativeToggleSetting.WORLDEDIT_ANYWHERE)) {
//            // Either remove or give them the fawe.bypass permission, depending on the new value
//            PermissionUser pexUser = PermissionsEx.getUser(player.getUniqueId().toString());
//            if ((boolean) event.getNewValue()) {
//                pexUser.addPermission("fawe.bypass", "Plots2");
//            } else {
//                pexUser.removePermission("fawe.bypass", "Plots2");
//            }

        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerBlockPlace(BlockPlaceEvent event) {
        Block block = event.getBlockPlaced();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        MinecraftProfile mcp = PlayerUtil.getProfile(event.getPlayer());

        // BlockData modifiers
        BlockData blockData = block.getBlockData();
        if (blockData instanceof Slab && !block.getType().equals(event.getBlockReplacedState().getType())) {
            // If flip_slabs is set to true then change the type of the placed slab block only if it isn't turning an
            // existing slab into a double slab
            if (CreativeToggleSetting.FLIP_SLABS.getValue(mcp)) {
                Slab slabData = (Slab) blockData;
                slabData.setType(Slab.Type.TOP);
                block.setBlockData(slabData);
            }
        } else if (blockData instanceof CraftLayeredCauldron || blockData instanceof CraftPowderSnowCauldron) {
            // If fill_cauldrons is set to true then update the cauldron's data
            if (CreativeToggleSetting.FILL_CAULDRONS.getValue(mcp)) {
                Levelled cauldronData = (Levelled) block.getBlockData();
                cauldronData.setLevel(3);
                block.setBlockData(cauldronData);
            }
        } else {
            // Block type modifiers
            switch (block.getType()) {
                case REDSTONE_TORCH:
                case REDSTONE_WALL_TORCH:
                    if (!event.getBlockAgainst().getType().equals(Material.REDSTONE_BLOCK)) {
                        return;
                    }

                    // If remove_torches_on_redstone_blocks is set to true then remove the torch after it pulses
                    if (CreativeToggleSetting.REMOVE_TORCHES_ON_REDSTONE_BLOCKS.getValue(mcp)) {
                        Bukkit.getServer().getScheduler().runTaskLater(SynergyCreative.getPlugin(), new Runnable() {
                            @Override
                            public void run() {
                                block.setType(Material.AIR);
                            }
                        }, 4L);
                    }
                    return;
                default:
                    break;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputActivate(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // Ignore the event if it wasn't a block that was right-clicked or stepped on
        if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK) && !event.getAction().equals(Action.PHYSICAL)) {
            return;
        }

        // Ignore the event if the block isn't a redstone input
        Block block = event.getClickedBlock();
        if (!Input.isValidInput(block.getType())) {
            return;
        }

        InputManager im = InputManager.getInstance();

        // Ignore the event if the block isn't an Input
        Input input = im.getInput(block);
        if (input == null) {
            return;
        }

        // Try to activate the input
        input.checkAndActivate(player, event);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputDeactivate(BlockRedstoneEvent event) {
        Block block = event.getBlock();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Ignore the event if the block isn't a redstone input
        if (!Input.isValidInput(block.getType())) {
            return;
        }

        // Ignore the event if the input isn't turning off
        if (event.getNewCurrent() != 0) {
            return;
        }

        InputManager im = InputManager.getInstance();

        // Ignore the event if the block isn't an Input
        Input input = im.getInput(block);
        if (input == null) {
            return;
        }
        // Try to deactivate the input
        input.deactivate();
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInputActivateWirelessly(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
            return;
        }

        // Ignore the event if it wasn't air that was right-clicked
        if (!event.getAction().equals(Action.RIGHT_CLICK_AIR) && !event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }

        // Ignore the event if an item stack wasn't involved
        ItemStack item = event.getItem();
        if (item == null || item.getType().equals(Material.AIR)) {
            return;
        }

        // Ignore the event if the item doesn't have the NBT value
        String activatorID = ItemUtil.getNBTString(item, "activatorID");
        if (activatorID.isEmpty()) {
            return;
        }

        // Try to activate all Inputs that have a WirelessInputFunction with the activatorID
        for (HashMap<String, Input> inputsForChunk : InputManager.getInstance().getInputs().values()) {
            for (Input input : inputsForChunk.values()) {
                for (InputFunction inputFunction : input.getInputFunctions()) {
                    if (inputFunction instanceof WirelessInputFunction) {
                        WirelessInputFunction wirelessInput = (WirelessInputFunction) inputFunction;
                        if (activatorID.equals(wirelessInput.getActivatorID())) {
                            input.checkAndActivate(player, event);
                        }
                    }
                }

                // Uncancel the event so that other Inputs can activate
                event.setCancelled(false);
            }
        }

        // Prevent the block and the item of the activator from being used
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputBreak(BlockBreakEvent event) {
        Block block = event.getBlock();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Ignore the event if the block isn't a redstone input
        if (!Input.isValidInput(block.getType())) {
            return;
        }

        InputManager im = InputManager.getInstance();

        // Ignore the event if the block isn't an Input
        Input input = im.getInput(block);
        if (input == null) {
            return;
        }

        Player player = event.getPlayer();
        boolean bypassing = player.hasPermission("syn.input.remove.bypass") && !PlotUtil.canBuild(player, input.getLocation());

        // Only allow the owner (and those with bypass perms) to break an Input
        if (PlotUtil.canBuild(player, input.getLocation()) || bypassing) {
            // Remove the input and notify the player
            im.deleteInput(input);

            player.sendMessage(Message.format("events.input_removal.info.removed_input"));
            // Warn the player if they are bypassing the owner restriction to remove the input
            if (bypassing) {
                player.sendMessage(Message.format("events.input_removal.warning.bypassed_ownership"));
            }
        } else {
            // Otherwise cancel the event and give the player an error
            player.sendMessage(Message.format("events.input_removal.error.no_permission"));
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputBreak(PlayerBucketEmptyEvent event) {
        Block block = event.getBlockClicked().getRelative(event.getBlockFace());

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Ignore the event if the block isn't a redstone input
        if (!Input.isValidInput(block.getType())) {
            return;
        }

        InputManager im = InputManager.getInstance();

        // Ignore the event if the block isn't an Input
        Input input = im.getInput(block);
        if (input == null) {
            return;
        }

        Player player = event.getPlayer();
        boolean bypassing = player.hasPermission("syn.input.remove.bypass") && !PlotUtil.canBuild(player, input.getLocation());

        // Only allow the owner (and those with bypass perms) to break an Input
        if (PlotUtil.canBuild(player, input.getLocation()) || bypassing) {
            // Remove the input and notify the player
            im.deleteInput(input);

            player.sendMessage(Message.format("events.input_removal.info.removed_input"));
            // Warn the player if they are bypassing the owner restriction to remove the input
            if (bypassing) {
                player.sendMessage(Message.format("events.input_removal.warning.bypassed_ownership"));
            }
        } else {
            // Otherwise cancel the event and give the player an error
            player.sendMessage(Message.format("events.input_removal.error.no_permission"));
            event.setCancelled(true);
        }
    }

//    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
//    public void onPlayerJoin(PlayerJoinEvent event) {
//        Player player = event.getPlayer();
//
//        // If the player no longer has permission to worldedit anywhere then remove the fawe.bypass permission node
//        if (!player.hasPermission(CreativeToggleSetting.WORLDEDIT_ANYWHERE.getPermission())) {
//            PermissionsEx.getUser(player.getUniqueId().toString()).removePermission("fawe.bypass", "Plots2");
//        }
//    }

    private void handleShulkerDrops(Block block, List<Block> blocks) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Ignore the event if it happens in a world with tile drops = true (the shulker will be handled normally)
        Boolean doTileDrops = block.getWorld().getGameRuleValue(GameRule.DO_TILE_DROPS);
        if (doTileDrops == null || doTileDrops) {
            return;
        }

        // Drop the shulker item in the next tick
        for (Block b : blocks) {
            if (b.getBlockData() instanceof CraftShulkerBox) {
                for (ItemStack item : b.getDrops()) {
                    Bukkit.getScheduler().runTask(SynergyCreative.getPlugin(), () -> b.getWorld().dropItem(b.getLocation(), item));
                }
            }
        }
    }
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPistonPushShulkerBox(BlockPistonExtendEvent event) {
        handleShulkerDrops(event.getBlock(), event.getBlocks());
    }
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPistonPushShulkerBox(BlockPistonRetractEvent event) {
        handleShulkerDrops(event.getBlock(), event.getBlocks());
    }

}
