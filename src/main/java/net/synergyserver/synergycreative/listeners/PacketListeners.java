package net.synergyserver.synergycreative.listeners;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.wrappers.EnumWrappers;
import net.minecraft.util.MathHelper;
import net.minecraft.world.entity.EnumMoveType;
import net.minecraft.world.entity.player.EntityHuman;
import net.minecraft.world.phys.Vec3D;
import net.synergyserver.synergycreative.SynergyCreative;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.WeatherType;
import org.bukkit.craftbukkit.v1_19_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_19_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Listens to packets.
 */
public class PacketListeners {

    // Handle the Poseidon trident
    public static final PacketListener handlePoseidon = new PacketAdapter(SynergyCreative.getPlugin(), ListenerPriority.NORMAL, PacketType.Play.Client.BLOCK_DIG) {
        @Override
        public void onPacketReceiving(PacketEvent event) {
            if (event.getPacket().getPlayerDigTypes().readSafely(0) != EnumWrappers.PlayerDigType.RELEASE_USE_ITEM) {
                return;
            }

            Player player = event.getPlayer();

            // Ignore the event if it doesn't take place in this plugin's world group
            if (!SynergyCreative.getWorldGroup().contains(player.getWorld().getName())) {
                return;
            }

            ItemStack tridentItem = player.getInventory().getItemInMainHand();

            // Ignore the event if the player isn't holding a trident
            if (tridentItem.getType() != Material.TRIDENT) {
                return;
            }

            // Set the range to be [0, 3] to avoid potentially lag-causing launching
            int riptideLevel = Math.max(0, Math.min(tridentItem.getEnchantmentLevel(Enchantment.RIPTIDE), 3));

            // If the trident doesn't have riptide then ignore the event
            if (riptideLevel == 0) {
                return;
            }

            EntityHuman entityhuman = ((CraftPlayer) player).getHandle();

            // Ignore if riptide would be handled by the server anyways or the player isn't in personal rain
            if (entityhuman.aS() /* isInWaterOrRain() */ || player.getPlayerWeather() != WeatherType.DOWNFALL) {
                return;
            }

            net.minecraft.world.item.ItemStack tridentNMSItem = CraftItemStack.asNMSCopy(tridentItem);

            // Minecraft start (ItemTrident) - Sync and apply riptide effect
            Bukkit.getScheduler().runTask(SynergyCreative.getPlugin(), () -> {
                // Damage the trident
//                tridentNMSItem.damage(1, entityhuman, (entityhuman1) -> {
//                    entityhuman1.broadcastItemBreak(entityhuman.getRaisedHand());
//                });
                tridentNMSItem.a(1, entityhuman, (entityhuman1) -> {
                    entityhuman1.d(entityhuman.eU());
                });

                // CraftBukkit start
                org.bukkit.event.player.PlayerRiptideEvent riptideEvent = new org.bukkit.event.player.PlayerRiptideEvent(player, CraftItemStack.asCraftMirror(tridentNMSItem));
                Bukkit.getScheduler().runTask(SynergyCreative.getPlugin(), () -> Bukkit.getServer().getPluginManager().callEvent(riptideEvent));
                // CraftBukkit end
                float f = entityhuman.dq(); // Yaw
                float f1 = entityhuman.ds(); // Pitch
                float f2 = -net.minecraft.util.MathHelper.a(f * 0.017453292F) * net.minecraft.util.MathHelper.b(f1 * 0.017453292F);
                float f3 = -net.minecraft.util.MathHelper.a(f1 * 0.017453292F);
                float f4 = net.minecraft.util.MathHelper.b(f * 0.017453292F) * net.minecraft.util.MathHelper.b(f1 * 0.017453292F);
                float f5 = MathHelper.c(f2 * f2 + f3 * f3 + f4 * f4);
                float f6 = 3.0F * ((1.0F + (float) riptideLevel) / 4.0F);

                f2 *= f6 / f5;
                f3 *= f6 / f5;
                f4 *= f6 / f5;
                entityhuman.i((double) f2, (double) f3, (double) f4);

                entityhuman.r(20);
                if (entityhuman.aw()) { // isOnGround()
                    float f7 = 1.1999999F;
                    entityhuman.a(EnumMoveType.a, new Vec3D(0.0, 1.1999999284744263, 0.0)); // move(), EnumMoveType.SELF
                }

                Sound sound;
                if (riptideLevel >= 3) {
                    sound = Sound.ITEM_TRIDENT_RIPTIDE_3;
                } else if (riptideLevel == 2) {
                    sound = Sound.ITEM_TRIDENT_RIPTIDE_2;
                } else {
                    sound = Sound.ITEM_TRIDENT_RIPTIDE_1;
                }
                player.getWorld().playSound(player.getLocation(), sound, SoundCategory.PLAYERS, 1.0F, 1.0F);
            });
        }
    };
}
