package net.synergyserver.synergycreative.listeners;

import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycreative.SynergyCreative;
import net.synergyserver.synergycreative.inputs.Input;
import net.synergyserver.synergycreative.inputs.InputManager;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.FaceAttachable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class WorldListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputActivate(EntityInteractEvent event) {
        Entity entity = event.getEntity();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(entity.getWorld().getName())) {
            return;
        }

        // Ignore the event if the block isn't even a redstone input
        Block block = event.getBlock();
        if (!Input.isValidInput(block.getType())) {
            return;
        }

        // Ignore the event if the block isn't an Input
        Input input = InputManager.getInstance().getInput(block);
        if (input == null) {
            return;
        }

        // Try to activate the input
        input.checkAndActivate(entity, event);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputBreak(BlockFromToEvent event) {
        Block block = event.getToBlock();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Ignore the event if the block isn't a redstone input
        if (!Input.isValidInput(block.getType())) {
            return;
        }

        InputManager im = InputManager.getInstance();

        // Ignore the event if the block isn't an Input
        Input input = im.getInput(block);
        if (input == null) {
            return;
        }

        // Prevent physics from affecting this input
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputBreak(EntityChangeBlockEvent event) {
        Block block = event.getBlock();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Ignore the event if the block isn't a redstone input
        if (!Input.isValidInput(block.getType())) {
            return;
        }

        InputManager im = InputManager.getInstance();

        // Ignore the event if the block isn't an Input
        Input input = im.getInput(block);
        if (input == null) {
            return;
        }

        // Prevent physics from affecting this input
        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputBreak(EntityExplodeEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(event.getEntity().getWorld().getName())) {
            return;
        }

        List<Block> blocks = event.blockList();

        // Ignore the event if no blocks were affected
        if (blocks == null || blocks.isEmpty()) {
            return;
        }

        // Prevent Inputs from being destroyed
        Iterator<Block> iterator = blocks.iterator();
        while (iterator.hasNext()) {
            Block block = iterator.next();

            // Ignore this block if it isn't a redstone input
            if (!Input.isValidInput(block.getType())) {
                continue;
            }

            InputManager im = InputManager.getInstance();

            // Ignore this block if it isn't an Input
            Input input = im.getInput(block);
            if (input == null) {
                continue;
            }

            // Prevent the Input from being removed
            iterator.remove();
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputPhysics(BlockPhysicsEvent event) {
        Block block = event.getBlock();

        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(block.getWorld().getName())) {
            return;
        }

        // Ignore the event if it's not air that's being updated
        if (!ItemUtil.isAir(event.getChangedType())) {
            return;
        }

        // Super ghetto fix due to the changed behavior of BlockPhysicsEvent in 1.13
        // If the old behavior returns where cancelling the event works then revert this back to that
        // If the block used to be an Input then spawn it back in after a tick
        Input input = InputManager.getInstance().getInput(block);
        if (input != null) {
            Bukkit.getScheduler().runTaskLater(SynergyCreative.getPlugin(), () -> {
                // Set the input's type
                block.setType(input.getBlockType(), false);

                // Set the input's orientation data
                BlockData blockData = block.getBlockData();
                if (blockData instanceof Directional && input.getFacing() != null) {
                    ((Directional) blockData).setFacing(input.getFacing());
                }
                if (blockData instanceof FaceAttachable && input.getFace() != null) {
                    ((FaceAttachable) blockData).setAttachedFace(input.getFace());
                }
                block.setBlockData(blockData, false);
            }, 0L);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInputPush(BlockPistonExtendEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(event.getBlock().getWorld().getName())) {
            return;
        }

        // Iterate over each block to see if an Input is going to be pushed
        for (Block block : event.getBlocks()) {
            // Ignore if the block isn't a redstone input
            if (!Input.isValidInput(block.getType())) {
                continue;
            }

            // If the block is an Input then cancel the event to prevent it from being broken
            Input input = InputManager.getInstance().getInput(block);
            if (input != null) {
                event.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChunkLoad(ChunkLoadEvent event) {
        // Call Input#load on every input in the chunk
        String chunkID = BukkitUtil.getChunkId(event.getChunk());
        HashMap<String, Input> inputsForChunk = InputManager.getInstance().getInputs().get(chunkID);
        if (inputsForChunk != null) {
            for (Input input : inputsForChunk.values()) {
                input.load();
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChunkUnload(ChunkUnloadEvent event) {
        // Call Input#unload on every input in the chunk
        String chunkID = BukkitUtil.getChunkId(event.getChunk());
        HashMap<String, Input> inputsForChunk = InputManager.getInstance().getInputs().get(chunkID);
        if (inputsForChunk != null) {
            for (Input input : inputsForChunk.values()) {
                input.unload();
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void fireworkPatchOnChunkLoad(ChunkLoadEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(event.getChunk().getWorld().getName())) {
            return;
        }

        // Indiscriminately remove fireworks as a temporary patch for the crash bug
        for (Entity e : event.getChunk().getEntities()) {
            if (e.getType().equals(EntityType.FIREWORK)) {
                e.remove();
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void fireworkPatchOnEntitySpawn(EntitySpawnEvent event) {
        // Ignore the event if it doesn't take place in this plugin's world group
        if (!SynergyCreative.getWorldGroup().contains(event.getEntity().getWorld().getName())) {
            return;
        }

        // Indiscriminately remove fireworks as a temporary patch for the crash bug
        if (event.getEntityType().equals(EntityType.FIREWORK)) {
            event.setCancelled(true);
        }
    }
}
