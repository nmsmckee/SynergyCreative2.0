package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "input",
        aliases = {"inputs", "redstoneinput", "redstoneinputs", "rsi", "synput", "notdamnspam", "notfuckingdamnspam"},
        permission = "syn.input",
        usage = "/input <add|remove|info>",
        description = "Main command for managing inputs.",
        minArgs = 1,
        validSenders = SenderType.PLAYER
)
public class InputCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
