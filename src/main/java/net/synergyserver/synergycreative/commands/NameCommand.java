package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.ItemUtil;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

@CommandDeclaration(
        commandName = "name",
        permission = "syn.name",
        usage = "/name [name]",
        description = "Names the item in your main hand with the provided name. " +
                "If no name is provided then this clears the name of the item.",
        validSenders = SenderType.PLAYER
)
public class NameCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        ItemStack item = player.getInventory().getItemInMainHand();

        // Check if the player has an item in their hand
        if (item == null || item.getType().equals(Material.AIR)) {
            player.sendMessage(Message.get("commands.name.error.item_not_found"));
            return false;
        }

        // If no args are given then clear the name on the item
        if (args.length == 0) {
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName("");
            item.setItemMeta(meta);

            player.sendMessage(Message.get("commands.name.info.removed"));
            return true;
        }
        // Otherwise set the name to the new name
        String newName = String.join(" ", args);
        // If the player has permission to use color codes in item names
        newName = Message.translateCodes(newName, player, "syn.name");

        ItemUtil.setName(item, newName);
        player.updateInventory();
        player.sendMessage(Message.format("commands.name.info.added", newName));
        return true;
    }
}
