package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycreative.inputs.Input;
import net.synergyserver.synergycreative.inputs.InputFunction;
import net.synergyserver.synergycreative.inputs.InputManager;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Set;

@CommandDeclaration(
        commandName = "info",
        permission = "syn.input.info",
        usage = "/input info [-type <function type>]",
        description = "Provides information about the managed input that you are looking at. " +
                "You can get more detailed information about particular functionality by specifying an input type.",
        maxArgs = 0,
        validSenders = SenderType.PLAYER,
        parseCommandFlags = true,
        parentCommandName = "input"
)
public class InputInfoCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        // If the player isn't looking at an input then give the player an error
        Block block = player.getTargetBlock((Set<Material>) null, 10);
        if (block == null || !Input.isValidInput(block.getType())) {
            player.sendMessage(Message.format("commands.input.error.input_not_found"));
            return false;
        }

        // Get the Input at that location and tell player if none was found
        Input input = InputManager.getInstance().getInput(block);
        if (input == null) {
            player.sendMessage(Message.format("commands.input.info.info.no_inputs"));
            return true;
        }

        HashMap<String, InputFunction> inputs = new HashMap<>();
        for (InputFunction inputFunction : input.getInputFunctions()) {
            inputs.put(inputFunction.getType(), inputFunction);
        }
        Set<String> inputTypes = inputs.keySet();
        String inputTypesList = Message.createFormattedList(
                inputTypes,
                Message.get("info_colored_lists.item_color_1"),
                Message.get("info_colored_lists.item_color_2"),
                Message.get("info_colored_lists.grammar_color")
        );

        // If no input type was specified then just print out basic info for the Input overall
        if (!flags.hasFlag("-type", "-t")) {
            player.sendMessage(Message.format("commands.input.info.info.basic_info", inputTypesList));
            return true;
        }

        String inputType = flags.getFlagValue("-type", "-t");

        // If they didn't provide an input type the give the player an error
        if (inputType.isEmpty()) {
            player.sendMessage(Message.format("commands.input.info.error.function_type_not_found"));
            return false;
        }

        // If they didn't provide an input type that the Input has a registered input of then give the player an error
        if (!inputTypes.contains(inputType)) {
            player.sendMessage(Message.format("commands.input.error.invalid_function_type", inputType, inputTypesList));
            return false;
        }

        // Give the player the detailed description of the requested input
        player.sendMessage(Message.format("commands.input.info.info.detailed_info", inputType, inputs.get(inputType).getDescription()));
        return true;
    }
}
