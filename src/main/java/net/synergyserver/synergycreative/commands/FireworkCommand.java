package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import org.bukkit.command.CommandSender;

@CommandDeclaration(
        commandName = "firework",
        aliases = "fw",
        permission = "syn.firework",
        usage = "/firework <meta|launch|clear|power>",
        description = "Main command for manipulating fireworks."
)
public class FireworkCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        return false;
    }
}
