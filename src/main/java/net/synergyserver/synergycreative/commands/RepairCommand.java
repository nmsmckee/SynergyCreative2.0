package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.configs.Message;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@CommandDeclaration(
        commandName = "repair",
        permission = "syn.repair",
        usage = "/repair [-all]",
        description = "Repairs items in the player's hand or inventory",
        parseCommandFlags = true,
        validSenders = SenderType.PLAYER
)

public class RepairCommand extends MainCommand {
    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        if(flags.hasFlag("-a", "-all")){
            for(ItemStack item: player.getInventory()) {
                if (!item.getType().equals(Material.AIR)) {
                    //player.sendMessage(item.getType().toString() + " : " + item.getDurability() + "/ " + item.getType().getMaxDurability());
                    item.setDurability(((short) 0));
                }
            }
            player.sendMessage(Message.get("commands.repair.info.repaired_all"));
        } else {
            ItemStack item = player.getInventory().getItemInMainHand();
            item.setDurability(((short) 0));
            player.sendMessage(Message.format("commands.repair.info.repaired_one",
                    item.getType().toString().toLowerCase().replace("_"," ")));
        }
        return true;
    }
}
