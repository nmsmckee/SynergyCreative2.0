package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;

@CommandDeclaration(
        commandName = "meta",
        permission = "syn.firework.meta",
        usage = "/firework meta <color|fade|shape|effect>",
        description = "Alters the meta of the firework in a player's hand",
        minArgs = 1,
        parentCommandName = "firework"
)
public class FireworkMetaCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;
        PlayerInventory inventory = player.getInventory();

        ItemStack fireworkItem = inventory.getItemInMainHand();
        FireworkMeta fireworkMeta = (FireworkMeta) fireworkItem.getItemMeta();

        if (!fireworkItem.getType().equals(Material.FIREWORK_ROCKET)) {
            player.sendMessage(Message.get("commands.firework.error.no_firework"));
            return false;
        }

        FireworkEffect.Builder builder = FireworkEffect.builder();

        boolean hasColor = false;

        for (String argument : args) {
            int colonIndex = argument.indexOf(':');
            String key = argument.substring(0, colonIndex);
            String value = argument.substring(colonIndex + 1);

            switch (key.toLowerCase()) {
                case "color":
                    Color primaryColor;
                    if(BukkitUtil.isValidColorCode(value)){
                        primaryColor = BukkitUtil.parseColorCode(value);
                    } else if (BukkitUtil.isValidColorName(value)){
                        primaryColor = BukkitUtil.parseColorName(value);
                    } else {
                        player.sendMessage(Message.format("commands.firework.meta.error.invalid_color", "color", value));
                        return false;
                    }

                    hasColor = true;
                    builder.withColor(primaryColor);
                    break;
                case "fade":
                    Color fadeColor;
                    if (BukkitUtil.isValidColorCode(value)) {
                        fadeColor = BukkitUtil.parseColorCode(value);
                    } else if (BukkitUtil.isValidColorName(value)){
                        fadeColor = BukkitUtil.parseColorName(value);
                    } else {
                        player.sendMessage(Message.format("commands.firework.meta.error.invalid_color", "fade", value));
                        return false;
                    }

                    hasColor = true;
                    builder.withFade(fadeColor);
                    break;

                case "shape":
                    if (!hasColor) {
                        player.sendMessage(Message.format("commands.firework.meta.error.no_color", "shape"));
                        return false;
                    }

                    boolean matched = false;
                    for (FireworkEffect.Type type : FireworkEffect.Type.values()) {
                        if (value.equalsIgnoreCase(type.toString())) {
                            builder.with(type);
                            matched = true;
                        }
                    }

                    if (!matched) {
                        player.sendMessage(Message.format("commands.firework.meta.error.invalid_shape", value));
                        return false;
                    }

                    break;

                case "effect":
                    if (!hasColor) {
                        player.sendMessage(Message.format("commands.firework.meta.error.no_color", "effect"));
                        return false;
                    }
                    switch (value.toLowerCase()) {

                        case "trail":

                            builder.trail(true);
                            break;

                        case "flicker":
                            builder.flicker(true);
                            break;

                        default:
                            player.sendMessage(Message.format("commands.firework.meta.error.invalid_effect", value));
                            return false;
                    }
                    break;
            }
        }

        fireworkMeta.addEffect(builder.build());
        fireworkItem.setItemMeta(fireworkMeta);
        player.sendMessage(Message.get("commands.firework.meta.info.built_rocket"));
        return true;
    }
}
