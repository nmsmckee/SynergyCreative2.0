package net.synergyserver.synergycreative.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.MainCommand;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.utils.MathUtil;
import net.synergyserver.synergycreative.utils.PlotUtil;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Jukebox;
import org.bukkit.block.data.Levelled;
import org.bukkit.block.data.type.Cake;
import org.bukkit.block.data.type.EndPortalFrame;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

@CommandDeclaration(
        commandName = "signal",
        aliases = {"sss", "signalstrength"},
        permission = "syn.signal",
        usage = "/signal <1-15>",
        description = "Sets the container the player is looking to output the specified signal strength through a comparator.",
        minArgs = 1,
        maxArgs = 1,
        validSenders = SenderType.PLAYER
)

public class SignalCommand extends MainCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Player player = (Player) sender;

        if (!MathUtil.isInteger(args[0])) {
            player.sendMessage(Message.format("commands.error.not_a_number", args[0]));
            return false;
        }

        int signalStrength = Integer.parseInt(args[0]);

        // Get the targeted block
        Block block = player.getTargetBlock(null, 5);

        // If the player cannot build where the Input is being created then give the player an error
        if (!PlotUtil.canBuild(player, block.getLocation())) {
            player.sendMessage(Message.format("commands.input.add.error.no_permission"));
            return false;
        }

        String typeName = block.getType().name().toLowerCase();

        //Handle misc comparator-able blocks
        switch (block.getType()) {
            case CAKE:
                //the range handling could be done in another function but I did it this way for readability

                if ((signalStrength % 2) == 1) {
                    player.sendMessage(Message.format("commands.error.not_even", signalStrength));
                    player.sendMessage(Message.format("commands.signal.info.cake_discrepency"));
                    return false;
                }

                //Fail if out of range for Cake
                if (signalStrength < 0 || signalStrength > 14) {
                    player.sendMessage(Message.format("commands.error.out_of_range", 0, 14, signalStrength));
                    return false;
                }

                //Get the actual cake
                Cake cake = (Cake) block.getBlockData();

                //Calculate the number of slices the cake needs
                int bites = cake.getMaximumBites() - (Math.floorDiv(signalStrength,2));

                //Set the number of slices
                cake.setBites(bites);
                block.setBlockData(cake);

                break;
            case CAULDRON:
            case LAVA_CAULDRON:
            case WATER_CAULDRON:
            case POWDER_SNOW_CAULDRON:
                //Fail if out of range for Cauldron
                if (signalStrength < 0 || signalStrength > 3) {
                    player.sendMessage(Message.format("commands.error.out_of_range", 0, 3, signalStrength));
                    return false;
                }

                Levelled cauldronData = (Levelled) block.getBlockData();
                cauldronData.setLevel(signalStrength);
                block.setBlockData(cauldronData);

                break;
            case END_PORTAL_FRAME:

                //Fail if out of range for end portal frame
                if (!(signalStrength == 0 || signalStrength == 15)) {
                    player.sendMessage(Message.format("commands.error.out_of_pair", 0, 15, signalStrength));
                    player.sendMessage(Message.format("commands.signal.info.ender_frame_discrepency"));
                    return false;
                }

                EndPortalFrame frame = (EndPortalFrame) block.getBlockData();
                frame.setEye(signalStrength == 15);
                block.setBlockData(frame);

                break;
            case JUKEBOX:

                //Fail if out of range for JukeBox
                if (signalStrength < 0 || signalStrength > 12) {
                    player.sendMessage(Message.format("commands.error.out_of_range", 0, 12, signalStrength));
                    return false;
                }

                Jukebox jukebox = (Jukebox) block.getState();

                if (signalStrength == 0) {
                    jukebox.setPlaying(null);
                    jukebox.setRecord(new ItemStack(Material.AIR));
                } else {
                    //get the value that the records start (and pray that they stay in order)
                    int startOrdinal = Material.MUSIC_DISC_13.ordinal() - 1;

                    Material newRecord = Material.values()[startOrdinal + signalStrength];

                    jukebox.setPlaying(newRecord);
                }

                jukebox.update();

                break;
            case BEACON:
                //Alert the player if they tried to signal a beacon as they are inventoryHolders that can not hold items
                //Fuck beacons
                player.sendMessage(Message.format("commands.signal.error.non_comparator_container", typeName));
                return false;
            default:
                // See if the block has an inventory, fill it
                if (block.getState() instanceof InventoryHolder) {

                    //Fail if out of range for inventoryHolders
                    if (signalStrength < 1 || signalStrength > 15) {
                        player.sendMessage(Message.format("commands.error.out_of_range", 1, 15, signalStrength));
                        return false;
                    }

                    Inventory blockInventory = ((InventoryHolder) block.getState()).getInventory();
                    int inventoryCapacity = 64 * blockInventory.getSize();
                    int fillerCount = (int) ((inventoryCapacity / 15.0) * signalStrength);

                    // Build the filler item
                    ItemStack filler = new ItemStack(Material.QUARTZ, fillerCount);
                    ItemMeta fillerMeta = filler.getItemMeta();
                    fillerMeta.setDisplayName(Message.format("commands.signal.filler_name"));
                    filler.setItemMeta(fillerMeta);

                    // Clear the blocks inventory and fill it with the right amount of items
                    blockInventory.clear();
                    blockInventory.addItem(filler);

                    // Create a block update for hoppers and stuff
                    block.getState().update();
                } else {
                    player.sendMessage(Message.format("commands.signal.error.not_container", typeName));
                    return false;
                }
        }

        // Give the player feedback
        player.sendMessage(Message.format("commands.signal.info.set_signal_strength", typeName, signalStrength));
        return true;
    }
}