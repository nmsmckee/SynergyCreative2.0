package net.synergyserver.synergycreative.inputs;

import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Transient;
import org.bukkit.entity.Entity;
import org.bukkit.event.Event;

/**
 * Represents an input with custom functionality.
 */
@Embedded
public abstract class InputFunction {

    @Transient
    private Input input;

    /**
     * Required constructor for Morphia to work.
     */
    public InputFunction() {}

    /**
     * Creates a new <code>InputFunction</code> with the given parameters.
     *
     * @param input The parent <code>Input</code>.
     */
    public InputFunction(Input input) {
        this.input = input;
    }

    /**
     * Gets the <code>Input</code> that this <code>InputFunction</code> is registered to.
     *
     * @return The <code>Input</code> of this <code>InputFunction</code>.
     */
    public Input getInput() {
        return input;
    }

    /**
     * Sets the <code>Input</code> that this <code>InputFunction</code> is registered to. Note: This has no impact on
     * the actual data stored in the database and is purely used to set the <code>Input</code> referenced for when
     * <code>InputFunctions</code> call their methods.
     *
     * @param input The <code>Input</code> of this <code>InputFunction</code>.
     */
    public void setInput(Input input) {
        this.input = input;
    }

    /**
     * Returns the human-readable type of this <code>InputFunction</code>.
     *
     * @return The type of this <code>InputFunction</code>.
     */
    public abstract String getType();

    /**
     * Returns a human-readable description of this <code>InputFunction</code>.
     *
     * @return A description of this <code>InputFunction</code>.
     */
    public abstract String getDescription();

    /**
     * Returns the priority of this <code>InputFunction</code>, used to determine its activation order. Lower numbers go first.
     *
     * @return The priority of this <code>InputFunction</code>.
     */
    public abstract InputPriorityLevel getPriority();

    /**
     * Determines whether this <code>InputFunction</code> should activate.
     *
     * @param entity The entity that is causing the activation or null if it is being activated by the plugin.
     * @param event The event that is causing the activation or null if it is being activated by the plugin.. Do not cancel the event from this method.
     * @return True if the <code>Input</code> should continue trying to activate.
     */
    public boolean shouldActivate(Entity entity, Event event) {
        return true;
    }

    /**
     * Called when the <code>Input</code> is going to activate.
     *
     * @param entity The entity is causing the activation or null if it is being activated by the plugin.
     */
    public void onInputActivate(Entity entity) {}

    /**
     * Called when the <code>Input</code> is going to deactivate.
     */
    public void onInputDeactivate() {}

    /**
     * Called when the <code>Input</code> is loaded into the world.
     */
    public void onInputLoad() {}

    /**
     * Called when the <code>Input</code> is unloaded from the world.
     */
    public void onInputUnload() {}
}
