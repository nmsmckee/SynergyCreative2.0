package net.synergyserver.synergycreative.inputs;

import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.PostLoad;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import net.synergyserver.synergycore.database.DataEntity;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.utils.BukkitUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.FaceAttachable;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Represents a physical redstone input block that has added custom functionality.
 */
@Entity(value = "inputs")
public class Input implements DataEntity {

    @Id
    private String id;

    @Property("o")
    private UUID owner;
    @Property("c")
    private long dateCreated;
    @Property("u")
    private long lastUsed;
    // The following data is used for quickly accessing this Input's type and for a data integrity check
    @Property("m")
    private Material blockType;
    @Property("f")
    private FaceAttachable.AttachedFace face;
    @Property("d")
    private BlockFace facing;


    @Property("i")
    private List<InputFunction> inputFunctions;

    @Transient
    private DataManager dm = DataManager.getInstance();

    /**
     * Required constructor for Morphia to work.
     */
    public Input() {}

    /**
     * Creates a new <code>Input</code> with the given parameters.
     *
     * @param owner The UUID of the player that created this input.
     * @param location The location of the input.
     */
    public Input(UUID owner, Location location) {
        this.id = BukkitUtil.blockLocationToString(location);

        this.owner = owner;
        this.dateCreated = System.currentTimeMillis();
        this.lastUsed = 0;

        Block block = location.getBlock();
        BlockData blockData = block.getBlockData();
        this.blockType = block.getType();
        if (blockData instanceof Directional) {
            this.facing = ((Directional) blockData).getFacing();
        }
        if (blockData instanceof FaceAttachable) {
            this.face = ((FaceAttachable) blockData).getAttachedFace();
        }

        this.inputFunctions = new ArrayList<>();
    }

    @Override
    public String getID() {
        return id;
    }

    /**
     * Gets the UUID of the player that created this input.
     *
     * @return The UUID of the owner.
     */
    public UUID getOwner() {
        return owner;
    }

    /**
     * Gets the location of this input.
     *
     * @return The location of this input.
     */
    public Location getLocation() {
        return BukkitUtil.blockLocationFromString(id);
    }

    /**
     * Gets the time of when this input was created.
     *
     * @return The time this input was created, in milliseconds since unix time.
     */
    public long getDateCreated() {
        return dateCreated;
    }

    /**
     * Gets the time of when this input was last used.
     *
     * @return The time this input was last used, in milliseconds since unix time.
     */
    public long getLastUsed() {
        return lastUsed;
    }

    /**
     * Sets the time of when this input was last used.
     *
     * @param lastUsed The time this input was last used, in milliseconds since unix time.
     */
    public void setLastUsed(long lastUsed) {
        this.lastUsed = lastUsed;
        dm.updateField(this, Input.class, "u", lastUsed);
    }

    /**
     * Registers an <code>InputFunction</code> to be handled at this <code>Input</code> and saves it to the database.
     *
     * @param inputFunction The <code>InputFunction</code> to register.
     */
    public void addInputFunction(InputFunction inputFunction) {
        inputFunctions.add(inputFunction);
        dm.updateField(this, Input.class, "i", inputFunctions);
    }

    /**
     * Unregisters an <code>InputFunction</code> from being handled at this
     * <code>Input</code> and removes it from the database.
     *
     * @param inputFunction The <code>InputFunction</code> to unregister.
     */
    public void removeInputFunction(InputFunction inputFunction) {
        inputFunctions.remove(inputFunction);
        dm.updateField(this, Input.class, "i", inputFunctions);
    }

    /**
     * Gets all <code>InputFunction</code>s of this <code>Input</code>.
     *
     * @return A list containing all <code>InputFunction</code>s.
     */
    public List<InputFunction> getInputFunctions() {
        return inputFunctions;
    }

    /**
     * Updates the field in the database.
     */
    public void saveInputFunctions() {
        dm.updateField(this, Input.class, "i", inputFunctions);
    }

    /**
     * Convenience method for getting the block represented by this <code>Input</code>.
     *
     * @return The block of this <code>Input</code>.
     */
    public Block getBlock() {
        return getLocation().getBlock();
    }

    /**
     * Gets the block type that this <code>Input</code> is supposed to be.
     *
     * @return The material of this <code>Input</code>.
     */
    public Material getBlockType() {
        return blockType;
    }

    /**
     * Gets the direction that this <code>Input</code> should be facing, or null if the input
     * block's data does not implement the <code>Directional</code> interface.
     *
     * @return The <code>BlockFace</code> of this <code>Input</code>.
     */
    public BlockFace getFacing() {
        return facing;
    }

    /**
     * Delete this function after data conversion
     */
    public void setFacing(BlockFace facing) {
        this.facing = facing;
        dm.updateField(this, Input.class, "d", facing);
    }

    /**
     * Gets the face that this <code>Input</code> is attached to, or null if the input
     * block's data does not implement the <code>Switch</code> interface.
     *
     * @return The <code>Face</code> of this <code>Input</code>.
     */
    public FaceAttachable.AttachedFace getFace() {
        return face;
    }

    /**
     * Delete this function after data conversion
     */
    public void setFace(FaceAttachable.AttachedFace face) {
        this.face = face;
        dm.updateField(this, Input.class, "f", face);
    }

    /**
     * Determines whether this this <code>Input</code> should activate.
     *
     * @param entity The entity that is causing the activation or null if it is being activated by the plugin.
     * @param event The event that is causing the activation or null if it is being activated by the plugin.
     * @return True if this <code>Input</code> should activate.
     */
    public boolean shouldActivate(org.bukkit.entity.Entity entity, Event event) {
        // Call shouldActivate on each of the Inputs based on their priority
        for (int i = InputPriorityLevel.values().length - 1; i >= 0; i--) {
            for (InputFunction inputFunction : inputFunctions) {
                // Ignore this inputFunction if it's not the priority that is being called
                if (!inputFunction.getPriority().equals(InputPriorityLevel.values()[i])) {
                    continue;
                }

                // If this Input shouldn't activate then cancel the event and exit
                if (!inputFunction.shouldActivate(entity, event)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Checks whether this <code>Input</code> should activate before doing so.
     *
     * @param entity The entity that caused the event or null if it is being activated by the plugin.
     * @param event The event that triggered the activation.
     */
    public void checkAndActivate(org.bukkit.entity.Entity entity, Cancellable event) {
        // If this Input is corrupted then remove it from the database and cancel the activation
        if (!getBlock().getType().equals(getBlockType())) {
            InputManager.getInstance().deleteInput(this);
            event.setCancelled(true);
            return;
        }

        // If this input shouldn't activate then cancel the event and return
        if (!shouldActivate(entity, (Event) event)) {
            event.setCancelled(true);
            return;
        }

        // Otherwise activate
        activate(entity);
    }

    /**
     * Forces the activation of this <code>Input</code> without calling <code>shouldActivate()</code>.
     *
     * @param entity The entity that caused the event or null if it is being activated by the plugin.
     */
    public void activate(org.bukkit.entity.Entity entity) {
        // Since the activation should go through, set the last activation time to now
        setLastUsed(System.currentTimeMillis());

        // Call onInputActivate on each input so they can handle the success
        for (InputFunction inputFunction : inputFunctions) {
            inputFunction.onInputActivate(entity);
        }
    }

    /**
     * This method should be called whenever the input of this <code>Input</code> deactivates.
     */
    public void deactivate() {
        // Call onInputDeactivate on each of the Inputs
        for (InputFunction inputFunction : inputFunctions) {
            inputFunction.onInputDeactivate();
        }
    }

    /**
     * Called when this <code>Input</code> is loaded into the world.
     */
    public void load() {
        // Call onInputLoad on each of the Inputs
        for (InputFunction inputFunction : inputFunctions) {
            inputFunction.onInputLoad();
        }
    }

    /**
     * Called when this <code>Input</code> is unloaded from the world.
     */
    public void unload() {
        // Call onInputUnload on each of the Inputs
        for (InputFunction inputFunction : inputFunctions) {
            inputFunction.onInputUnload();
        }
    }

    /**
     * This method is called during the Morphia lifecycle and is used to tell the <code>InputFunction</code>s
     * of this <code>Input</code> that this <code>Input</code> owns them.
     */
    @PostLoad
    public void initializeInputFunctions() {
        for (InputFunction inputFunction : inputFunctions) {
            inputFunction.setInput(this);
        }
    }

    /**
     * Checks whether the given type of block is able to be handled as an <code>Input</code>.
     *
     * @param material The material of the block to check.
     * @return True if the block is able to be registered.
     */
    public static boolean isValidInput(Material material) {
        switch (material) {
            case STONE_BUTTON:
            case OAK_BUTTON:
            case BIRCH_BUTTON:
            case SPRUCE_BUTTON:
            case JUNGLE_BUTTON:
            case DARK_OAK_BUTTON:
            case ACACIA_BUTTON:
            case CRIMSON_BUTTON:
            case WARPED_BUTTON:
            case POLISHED_BLACKSTONE_BUTTON:
            case LEVER:
            case STONE_PRESSURE_PLATE:
            case OAK_PRESSURE_PLATE:
            case BIRCH_PRESSURE_PLATE:
            case SPRUCE_PRESSURE_PLATE:
            case JUNGLE_PRESSURE_PLATE:
            case DARK_OAK_PRESSURE_PLATE:
            case ACACIA_PRESSURE_PLATE:
            case HEAVY_WEIGHTED_PRESSURE_PLATE:
            case LIGHT_WEIGHTED_PRESSURE_PLATE:
            case CRIMSON_PRESSURE_PLATE:
            case WARPED_PRESSURE_PLATE:
            case POLISHED_BLACKSTONE_PRESSURE_PLATE:
                return true;
            default:
                return false;
        }
    }
}
