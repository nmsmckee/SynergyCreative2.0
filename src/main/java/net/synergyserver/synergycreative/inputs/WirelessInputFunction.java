package net.synergyserver.synergycreative.inputs;

import com.plotsquared.bukkit.util.BukkitUtil;
import com.plotsquared.core.plot.Plot;
import dev.morphia.annotations.Embedded;
import dev.morphia.annotations.Property;
import dev.morphia.annotations.Transient;
import net.synergyserver.synergycore.configs.Message;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

@Embedded
public class WirelessInputFunction extends InputFunction {

    @Property("u")
    private UserGroup userGroup;
    @Property("a")
    private String activatorID;
    @Property("at")
    private Material activatorType;

    @Transient
    private boolean bypassedGroup = false;
    @Transient
    private boolean wasWirelesslyActivated = false;

    /**
     * Required constructor for Morphia to work.
     */
    public WirelessInputFunction() {}

    /**
     * Creates a new <code>WirelessInputFunction</code> with the given parameters.
     *
     * @param input The <code>Input</code> that this <code>WirelessInputFunction</code> is registered to.
     * @param userGroup The <code>UserGroup</code> that is allowed to use the input wirelessly.
     * @param activatorID The ID of the <code>ItemStack</code> used to wirelessly activate this input.
     * @param activatorType The type of the <code>ItemStack</code> used to wirelessly activate this input.
     */
    public WirelessInputFunction(Input input, UserGroup userGroup, String activatorID, Material activatorType) {
        super(input);
        this.userGroup = userGroup;
        this.activatorID = activatorID;
        this.activatorType = activatorType;
    }

    /**
     * Gets the activatorID of the <code>ItemStack</code> used to wirelessly activate this input.
     *
     * @return The activatorID of the activator of this input.
     */
    public String getActivatorID() {
        return activatorID;
    }

    @Override
    public String getType() {
        return "wireless";
    }

    @Override
    public String getDescription() {
        return Message.format("inputs.wireless.description", activatorType.name().toLowerCase().replace("_", " "), userGroup.toString());
    }

    @Override
    public InputPriorityLevel getPriority() {
        return InputPriorityLevel.HIGH;
    }

    @Override
    public boolean shouldActivate(Entity entity, Event event) {
        // Reset transient values
        bypassedGroup = false;
        wasWirelesslyActivated = false;

        // If the event isn't a PlayerInteractEvent then allow the activation
        if (event == null || !(event instanceof PlayerInteractEvent)) {
            return true;
        }

        PlayerInteractEvent playerInteractEvent = (PlayerInteractEvent) event;
        boolean rightClickedAir = playerInteractEvent.getAction().equals(Action.RIGHT_CLICK_AIR);
        boolean rightClickedOtherBlock = playerInteractEvent.getAction().equals(Action.RIGHT_CLICK_BLOCK) &&
                !playerInteractEvent.getClickedBlock().getLocation().equals(getInput().getLocation());

        // If it wasn't a player right trying to activate this InputFunction wirelessly then allow the activation
        if (!rightClickedAir && !rightClickedOtherBlock) {
            return true;
        }

        Player player = (Player) entity;
        Plot plot = Plot.getPlot(BukkitUtil.adapt(getInput().getLocation()));

        // If the player is in the right user group then allow the activation
        switch (userGroup) {
            case EVERYONE:
                wasWirelesslyActivated = true;
                return true;
            case PLOT_MEMBERS:
                // If they are a plot member then allow the activation
                if (plot != null && plot.getMembers().contains(player.getUniqueId())) {
                    wasWirelesslyActivated = true;
                    return true;
                }
            case PLOT_TRUSTED:
                // If they are a plot trusted then allow the activation
                if (plot != null && plot.getTrusted().contains(player.getUniqueId())) {
                    wasWirelesslyActivated = true;
                    return true;
                }
            case OWNER:
                // If they are the owner then allow the activation
                if (player.getUniqueId().equals(getInput().getOwner())) {
                    wasWirelesslyActivated = true;
                    return true;
                }

                // If they have bypass perms then allow the activation and warn them
                if (player.hasPermission("syn.input.wireless.bypass")) {
                    bypassedGroup = true;
                    wasWirelesslyActivated = true;
                    return true;
                }
            // By default cancel the activation and send the player an error
            default:
                Location loc = getInput().getLocation();
                player.sendMessage(Message.format("inputs.wireless.error.wrong_group", loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
                return false;
        }
    }

    @Override
    public void onInputActivate(Entity entity) {
        // Ignore if the input wasn't activated wirelessly
        if (!wasWirelesslyActivated) {
            return;
        }

        // Send a warning to the player that activated the input if they bypassed the user group restriction
        if (bypassedGroup) {
            entity.sendMessage(Message.format("inputs.wireless.warning.bypassed_group"));
        }

        // Activate the input in the world
        InputManager.getInstance().activateInputInWorld(getInput().getBlock());
    }
}
