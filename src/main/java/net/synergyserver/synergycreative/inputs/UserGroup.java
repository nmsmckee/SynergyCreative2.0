package net.synergyserver.synergycreative.inputs;

/**
 * Used to represent a group of users. Staff with an override
 * permission may be included for all except UserGroup.NOBODY.
 */
public enum UserGroup {
    NOBODY, OWNER, PLOT_TRUSTED, PLOT_MEMBERS, EVERYONE;

    @Override
    public String toString() {
        return name().toLowerCase().replace("_", " ");
    }
}
