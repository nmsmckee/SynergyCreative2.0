package net.synergyserver.synergycreative.inputs;

/**
 * Used to determine the activation order of <code>InputFunction</code>s. It should be noted that the activation order
 * determined by this enum is the opposite of Bukkit's <code>EventPriority</code>.
 */
public enum InputPriorityLevel {
    LOW, MEDIUM, HIGH
}
